package ru.pisarev.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pisarev.tm.endpoint.SessionDto;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.exception.system.AccessDeniedException;
import ru.pisarev.tm.service.SessionService;

public abstract class AbstractListener {

    @Nullable
    @Autowired
    protected SessionService sessionLocator;

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void handler(@NotNull final ConsoleEvent event);

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = name();
        @Nullable final String arg = arg();
        @Nullable final String description = description();

        if (!name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "] ";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

    @Nullable
    protected SessionDto getSession() {
        @Nullable SessionDto session = sessionLocator.getSession();
        if (session == null) throw new AccessDeniedException();
        return session;
    }

}
