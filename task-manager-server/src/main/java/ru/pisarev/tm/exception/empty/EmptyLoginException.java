package ru.pisarev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    @NotNull
    public EmptyLoginException() {
        super("Error. Login is empty");
    }

}
