package ru.pisarev.tm.api.repository.dto;

import ru.pisarev.tm.api.IRecordRepository;
import ru.pisarev.tm.dto.ProjectRecord;

import java.util.List;

public interface IProjectRecordRepository extends IRecordRepository<ProjectRecord> {

    void update(final ProjectRecord project);

    ProjectRecord findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<ProjectRecord> findAllByUserId(final String userId);

    ProjectRecord findByName(final String userId, final String name);

    ProjectRecord findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

}
