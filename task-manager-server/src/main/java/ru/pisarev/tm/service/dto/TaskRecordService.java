package ru.pisarev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.pisarev.tm.api.repository.dto.ITaskRecordRepository;
import ru.pisarev.tm.api.service.dto.ITaskRecordService;
import ru.pisarev.tm.dto.TaskRecord;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.repository.dto.TaskRecordRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public final class TaskRecordService extends AbstractRecordService<TaskRecord> implements ITaskRecordService {

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskRecord> findAll() {
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            return repository.findAll();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<TaskRecord> collection) {
        if (collection == null) return;
        for (TaskRecord item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord add(@Nullable final TaskRecord entity) {
        if (entity == null) return null;
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.add(entity);
            repository.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.clear();
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final TaskRecord entity) {
        if (entity == null) return;
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeById(entity.getId());
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            return repository.findByIndex(userId, index);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            return repository.findByName(userId, name);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeByIndex(userId, index);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeByName(userId, name);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final TaskRecord task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final TaskRecord task = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final TaskRecord task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final TaskRecord task = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final TaskRecord task = Optional.ofNullable(repository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final TaskRecord task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final TaskRecord task = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final TaskRecord task = Optional.ofNullable(repository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @SneakyThrows
    @Nullable
    public TaskRecord add(String user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskRecord task = new TaskRecord(name, description);
        add(user, task);
        return (task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskRecord> findAll(@NotNull final String userId) {
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            return repository.findAllByUserId(userId);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<TaskRecord> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();
            addAll(collection);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord add(final String user, @Nullable final TaskRecord entity) {
        if (entity == null) return null;
        entity.setUserId(user);
        @Nullable final TaskRecord entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.clearByUserId(userId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final TaskRecord entity) {
        if (entity == null) return;
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeByIdUserId(userId, entity.getId());
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
